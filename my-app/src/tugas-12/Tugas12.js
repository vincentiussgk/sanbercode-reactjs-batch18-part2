import React, {Component} from 'react';
class Lists extends Component{

    constructor(props){
      super(props)
      this.state ={
        tabelHargaBuah : [
            ['Semangka',10000 ,'1 kg'] ,
            [ 'Anggur', 40000, '0.4 kg'],
            [ 'Strawberry', 30000, '0.5 kg'],
            [ 'Jeruk', 30000, '1 kg'],
            [ 'Mangga', 30000, '0.5 kg']                  
        ],
        openForm : -1
      }
    }
    
    DeleteBaris(event){
        event.preventDefault();
        var temp = this.state.tabelHargaBuah.filter(function(value, index) {
                return index!=event.target.value;
            });
        this.setState({tabelHargaBuah: temp});
    }

    EditBaris(event){
        event.preventDefault();
        this.setState({openForm : event.target.value});
    }
    UpdateBaris(event){
        event.preventDefault();
    }
  
    render(){
      return(
        <>
          <h1>Tabel Harga Buah</h1>
          <table class="tableborder">
            <thead>
              <tr>
                <th class="border header name">Nama</th>
                <th class="border header price">Harga</th>
                <th class="border header price">Berat</th>
                <th class="border header price">Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  this.state.tabelHargaBuah.map((val, index)=>{
                    
                    return(                    
                      <tr>
                        
                        <td class="border elements">{val[0]}</td>
                        <td class="border elements">{val[1]}</td>
                        <td class="border elements">{val[2]}</td>
                        <td>
                            <form onSubmit={this.handleSubmit}>
                                <button value={index} onClick={(ev) => this.EditBaris(ev)}>Edit</button>
                                <button value={index} onClick={(ev) => this.DeleteBaris (ev)}>Delete</button>
                                {(this.state.openForm==index)? 
                                <div>
                                        <input type="text" value={val[0]}>
                                        
                                        </input>
                                        <input type="text" value={val[1]}>
                                        
                                        </input>
                                        <input type="text" value={val[2]}>
                                        
                                        </input>
                                        <form>
                                            <button value={index} onClick={(ev) => this.UpdateBaris (ev)}>Update</button>
                                        </form>
                                </div>        
                                :""}                              
                            </form>                            
                        </td>
                      </tr>
                      
                    )
                  })
                  
                }
            </tbody>
          </table>
  

        </>
      )
    }
  }
class Tugas12 extends React.Component{
    render(){
        return (
            <div>
                <Lists />
            </div>
        )
    }
}

export default Tugas12;